package com.carolinecady.discordiancalendar;

import androidx.appcompat.app.AppCompatActivity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import androidx.gridlayout.widget.GridLayout;
import android.widget.TextView;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;


/*
To do:
Okay I THINK st. tib's day works correctly now, even if it's kinda horribly kludged together so like. we're good? probably?

clean up ugly ass code a little

 */

public class MainActivity extends AppCompatActivity {

    android.widget.TextView dateText1, dateText2;
    android.widget.Button switchModeButton, prevButton, nextButton;
    android.widget.Spinner monthSelect, yearSelect;
    String[] discordianMonths = {"Chaos", "Discord", "Confusion", "Bureaucracy", "Aftermath"};
    String[] discordianWeekdays = {"Sweetmorn", "Boomtime", "Pungenday", "Prickle-\nPrickle", "Setting \nOrange"};
    String[] gregorianWeekdays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    String[] discordianHolidays = {"Mungday", "Chaoflux", "Mojoday", "Discoflux", "Syaday", "Confuflux", "Zaraday", "Bureflux", "Maladay", "Afflux", "St. Tib's Day"};
    ArrayAdapter<String> monthAdapter;
    ArrayAdapter<Integer> yearAdapter;
    GridLayout calendarGrid;
    GregorianCalendar today, focusDay;
    boolean isDiscordian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        today = (GregorianCalendar) Calendar.getInstance();
        focusDay = (GregorianCalendar) Calendar.getInstance();

        dateText1 = findViewById(R.id.dateText1);
        dateText2 = findViewById(R.id.dateText2);
        switchModeButton = findViewById(R.id.switchModeButton);
        prevButton = findViewById(R.id.prevButton);
        nextButton = findViewById(R.id.nextButton);
        monthSelect = findViewById(R.id.monthSelect);
        yearSelect = findViewById(R.id.yearSelect);

        calendarGrid = findViewById(R.id.calendarGrid);

        prevButton.setText("<"); //this is bc there were issues w hardcoding it to have "<" character

        monthAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new ArrayList<String>());
        monthSelect.setAdapter(monthAdapter);

        yearAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, new ArrayList<Integer>());
        yearSelect.setAdapter(yearAdapter);

        switchModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDiscordian) {
                    switchGregorian();
                } else {
                    switchDiscordian();
                }
            }
        });

        //Decrement month when prev button clicked
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { setMonth(-1); }
        });

        //Increment month when next button clicked
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { setMonth(1); }
        });

        //When month selected in spinner, switch to that month
        monthSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int currentMonth = (isDiscordian ? discordianMonthNum(focusDay) : focusDay.get(Calendar.MONTH));
                if (i != currentMonth) { setMonth(i - currentMonth); }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        //When year selected in spinner, switch to that year
        yearSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 5) {
                    focusDay.add(Calendar.YEAR, i - 5);
                    if (isDiscordian) { switchDiscordian(); } else { switchGregorian(); }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        switchDiscordian();
    }

    //INCREMENTS/DECREMENTS MONTH (either by 1 or within same year, otherwise there'll be problems w St Tibs Day)
    void setMonth(int monthsToAdd) {
        if (isDiscordian) {
            int dayOfYear = focusDay.get(Calendar.DAY_OF_YEAR);
            //Leap day stuff
            int i = 0;
            if ((focusDay.isLeapYear(focusDay.get(Calendar.YEAR)) && dayOfYear <= 60 && monthsToAdd >= 1)
                    || ((focusDay.isLeapYear(focusDay.get(Calendar.YEAR) + 1)) && dayOfYear >= 352 && monthsToAdd >= 1)) {
                i++;
            } else if (focusDay.isLeapYear(focusDay.get(Calendar.YEAR)) && dayOfYear >= 60 && (dayOfYear + (monthsToAdd * 73) <= 60)) {
                i--;
            }
            focusDay.add(Calendar.DATE, (monthsToAdd * 73) + i);
            switchDiscordian();
        } else {
            focusDay.add(Calendar.MONTH, monthsToAdd);
            switchGregorian();
        }
    }

    //Switch to Discordian calendar mode
    void switchDiscordian() {
        isDiscordian = true;
        dateText1.setText("Discordian Calendar");
        dateText2.setText("Today's date: " + discordianYear(today) + " " + discordianMonthName(today) + " " + (isLeapDay(today) ? "St. Tib's Day" : discordianDayOfMonth(today)));

        fillCalendarGrid(discordianWeekdays, 73, (discordianMonthNum(focusDay) * 73) % 5);
        setSpinnerMode();
    }

    //Switch to Gregorian calendar mode
    void switchGregorian() {
        isDiscordian = false;
        dateText1.setText("Gregorian Calendar");
        dateText2.setText("Today's date: " + new SimpleDateFormat("yyyy MMMM dd").format(today.getTime()));

        Calendar c = (Calendar) focusDay.clone();
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.get(Calendar.DAY_OF_WEEK);

        fillCalendarGrid(gregorianWeekdays, focusDay.getActualMaximum(Calendar.DAY_OF_MONTH), c.get(Calendar.DAY_OF_WEEK) - 1);
        setSpinnerMode();
    }

    //Set spinner contents
    void setSpinnerMode() {
        monthAdapter.clear();
        yearAdapter.clear();
        int current;
        if (isDiscordian) {
            monthAdapter.addAll(Arrays.asList(discordianMonths));
            monthSelect.setSelection(discordianMonthNum(focusDay));
            current = discordianYear(focusDay);
        } else {
            monthAdapter.addAll(Arrays.asList(DateFormatSymbols.getInstance().getMonths()));
            monthSelect.setSelection(focusDay.get(Calendar.MONTH));
            current = focusDay.get(Calendar.YEAR);
        }
        for (int i = current - 5; i < current + 6; i++) {
            yearAdapter.add(i);
        }
        yearSelect.setSelection(5);

    }

    //Fill in calendar grid
    void fillCalendarGrid(String[] weekdays, int monthLength, int firstWeekDay) {

        calendarGrid.removeAllViews();
        calendarGrid.setColumnCount(weekdays.length);

        //Get width info
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        //Fill in weekdays
        for (int i = 0; i < weekdays.length; i++) {
            TextView weekdayText = new TextView(this);
            calendarGrid.addView(weekdayText);
            weekdayText.setText(weekdays[i]);

            weekdayText.setGravity(Gravity.CENTER);
            ((GridLayout.LayoutParams) weekdayText.getLayoutParams()).setGravity(Gravity.CENTER);

            weekdayText.setTextSize(13);
            weekdayText.setTextColor(Color.parseColor("#000000"));
            ((GridLayout.LayoutParams) weekdayText.getLayoutParams()).setMargins(10, 0, 10, 15);
        }

        //Some extra squares after end of month to make calendar nice and rectangular
        int daysInFinalWeek = (monthLength + firstWeekDay) % weekdays.length;
        int nextMonthSquares = (weekdays.length - daysInFinalWeek) % weekdays.length;

        //Make date object for date represented by i
        GregorianCalendar curDate = (GregorianCalendar) focusDay.clone();
        if (isDiscordian && focusDay.isLeapYear(focusDay.get(Calendar.YEAR)) && focusDay.get(Calendar.DAY_OF_YEAR) >= 60 &&focusDay.get(Calendar.DAY_OF_YEAR) <= 74) {
            curDate.add(Calendar.DATE, -1);
        }
        curDate.add(Calendar.DATE, (1 - firstWeekDay) - (isDiscordian ? discordianDayOfMonth(focusDay) : focusDay.get(Calendar.DAY_OF_MONTH)));

        //Make and fill in day squares
        for (int i = 1 - firstWeekDay; i <= monthLength + nextMonthSquares; i++) {

            //Make TextView, set text to day number
            TextView dayText = new TextView(this);
            if (i >= 1 && i <= monthLength) {
                dayText.setText(""+i);
            }

            //Add TextView to calendarGrid
            calendarGrid.addView(dayText);

            //Set aesthetic stuff for TextView
            ((GridLayout.LayoutParams) dayText.getLayoutParams()).setGravity(Gravity.CENTER);
            dayText.setGravity(Gravity.CENTER);
            dayText.setTypeface(null, Typeface.BOLD);
            dayText.setBackgroundResource(R.drawable.text_background);
            dayText.getLayoutParams().width = width / (isDiscordian ? 5 : 7);
            dayText.getLayoutParams().height = isDiscordian ? 60 : 160;
            dayText.setTextColor(Color.parseColor("#555555"));

            //Set OnClickListener to switch focusDay when a day is clicked
            int curMonth = curDate.get(Calendar.MONTH);
            int curDayOfMonth = curDate.get(Calendar.DAY_OF_MONTH);
            dayText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    focusDay.set(Calendar.MONTH, curMonth);
                    focusDay.set(Calendar.DAY_OF_MONTH, curDayOfMonth);
                    if (isDiscordian) { switchDiscordian(); } else { switchGregorian(); }
                }
            });

            //Highlights focusday in a color
            if (i == (isDiscordian ? discordianDayOfMonth(focusDay) : focusDay.get(Calendar.DAY_OF_MONTH))) {
                dayText.setBackgroundColor(Color.parseColor("#FFD0FF"));
            }
            //Asterisks around current day
            if (isDiscordian ? (i == discordianDayOfMonth(today) && discordianMonthNum(today) == discordianMonthNum(focusDay) && today.get(Calendar.YEAR) == focusDay.get(Calendar.YEAR))
                    : (i == today.get(Calendar.DAY_OF_MONTH) && today.get(Calendar.MONTH) == focusDay.get(Calendar.MONTH)) && today.get(Calendar.YEAR) == focusDay.get(Calendar.YEAR)) {
                dayText.setText("~" + i + "~");
            }

            //Does stuff when it's a holiday
            if (discordianDayOfMonth(curDate) == 5) {
                dayText.setTextColor(Color.parseColor("#FF0000"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    dayText.setTooltipText(discordianHolidays[discordianMonthNum(curDate) * 2]);
                }
            } else if (discordianDayOfMonth(curDate) == 50) {
                dayText.setTextColor(Color.parseColor("#FF0000"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    dayText.setTooltipText(discordianHolidays[discordianMonthNum(curDate) * 2 + 1]);
                }
            } else if (isLeapDay(curDate) && !isDiscordian) {
                dayText.setTextColor(Color.parseColor("#FF0000"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    dayText.setTooltipText("+" + discordianHolidays[10]);
                }
            }

            //Increment date
            curDate.add(Calendar.DATE, 1);

            //St. Tib's day stuff
            //May have issues if you try to use the app on St.Tib's day but I don't care anymore. It's a little easter egg. It's a feature, not a bug
            if(isLeapDay(curDate) && isDiscordian){
                curDate.add(Calendar.DATE, 1);
                dayText.setTextColor(Color.parseColor("#FF0000"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    dayText.setTooltipText("+" + discordianHolidays[10]);
                }
            }

        }

    }

    //DISCORDIAN DATE FUNCTIONS

    int discordianYear(GregorianCalendar date) {
        return date.get(Calendar.YEAR) + 1166;
    }

    int discordianMonthNum(GregorianCalendar date) {
        GregorianCalendar gdate = (GregorianCalendar)date;
        if (date.isLeapYear(date.get(Calendar.YEAR)) && date.get(Calendar.DAY_OF_YEAR) >= 60) {
            //if it is on or after the leap day
            return (date.get(Calendar.DAY_OF_YEAR) - 2) / 73;
        } else {
            //1st day of year = 1, 1-73 return 0, 74-146 -> 1 etc
            return (date.get(Calendar.DAY_OF_YEAR) - 1) / 73;
        }
    }

    String discordianMonthName(GregorianCalendar date) {
        return discordianMonths[discordianMonthNum(date)];
    }

    //returns 59 on st.tib's day
    int discordianDayOfMonth(GregorianCalendar date) {
        if (date.isLeapYear(date.get(Calendar.YEAR))) {
            if (date.get(Calendar.DAY_OF_YEAR) == 60) {
                return 59;
            } else if (date.get(Calendar.DAY_OF_YEAR) > 60) {
                return ((date.get(Calendar.DAY_OF_YEAR) - 2) % 73) + 1;
            }
        }
        return ((date.get(Calendar.DAY_OF_YEAR) - 1) % 73) + 1;
    }

    boolean isLeapDay(GregorianCalendar date) {
        return date.isLeapYear(focusDay.get(Calendar.YEAR)) && date.get(Calendar.MONTH) == 1 && date.get(Calendar.DAY_OF_MONTH) == 29;
    }

}
//fnord